#!/usr/bin/env python3
import glob
import time
import sys
import logging

import numpy as np
import cv2

def capture_calibration():
    cam = cv2.VideoCapture(0)
    cam.set(cv2.CAP_PROP_FPS, 60);
    cam.set(cv2.CAP_PROP_FRAME_WIDTH, 320);
    cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 240);
    # cam.set(cv2.CAP_PROP_FRAME_WIDTH, 640);
    # cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 480);

    for i in range(0, 20):
        for n in range(0, 10):
            ret_val, img = cam.read()
        time.sleep(0.1)
        ret_val,img = cam.read()
        cv2.imshow("webcam", img)
        cv2.imwrite("calib_{}.jpg".format(i), img)
        if cv2.waitKey(1) == 27:
            break
        input()
        print("next loop")
    cv2.destroyAllWindows()



def calibrate():
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    dimensions = (9, 6)

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((dimensions[0]*dimensions[1],3), np.float32)
    objp[:,:2] = np.mgrid[0:dimensions[0],0:dimensions[1]].T.reshape(-1,2)
    # Assume 0.03 meter chessboard square size
    objp *= 0.03

    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.

    images = glob.glob('calib_*.jpg')

    print("Starting loop")
    for fname in images:
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, dimensions, None)

        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)

            corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
            imgpoints.append(corners2)

            # Draw and display the corners
            img = cv2.drawChessboardCorners(img, dimensions, corners2,ret)
            cv2.imshow('img',img)
            cv2.waitKey(500)
        else:
            cv2.imshow('img',gray)
            cv2.waitKey(500)
            print("Found no chessboard")

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

    print(tvecs)
    print("RVecs: ", rvecs)
    print(rvecs)
    np.save("camera_matrix", mtx)
    np.save("camera_distortion", dist)

    cv2.destroyAllWindows()


def run_tracker():
    log = logging.getLogger()
    show_gui = False
    addr = None
    if len(sys.argv) == 1:
        log.warn("No remote address address specified, running GUI")
        show_gui = True
    elif len(sys.argv) == 2:
        addr = sys.argv[1]
        log.info("Connecting to: {}".format(addr))

    camera_matrix = np.load("camera_matrix.npy")
    camera_distortion = np.load("camera_distortion.npy")

    cam = cv2.VideoCapture(0)
    cam.set(cv2.CAP_PROP_FRAME_WIDTH, 320);
    cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 240);
    cam.set(cv2.CAP_PROP_FPS, 60);

    # Setup SimpleBlobDetector parameters.
    params = cv2.SimpleBlobDetector_Params()

    # Change thresholds
    params.minThreshold = 10
    params.maxThreshold = 200


    # Filter by Area.
    params.filterByArea = False
    # params.minArea = 1500

    # Filter by Circularity
    params.filterByCircularity = False
    # params.minCircularity = 0.1

    # Filter by Convexity
    params.filterByConvexity = False
    # params.minConvexity = 0.87

    # Filter by Inertia
    # params.filterByInertia = True
    params.minInertiaRatio = 0.01
    detector = cv2.SimpleBlobDetector_create(params);

    tracker_points = np.array([
        [-30, 60, 0],
        [8, 20, 0], # Center point
        [68, 30, 15], # Bottom point
        [100, 50, 0], # Bottom point
    ], dtype='f') / 1000

    last_print = time.time();

    while True:
        (ret_val, img) = cam.read()
        # img = cv2.undistort(img, camera_matrix, camera_distortion)
        # cv2.imshow("raw", img)

        # hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


        (_, thresholded) = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
        # Filter the colour of the LEDs
        # lower_bound = (120, 0, 0);
        # upper_bound = (190, 255, 255);
        # filtered = cv2.inRange(hsv, lower_bound, upper_bound)
        # masked = cv2.bitwise_and(img, img, mask=filtered)
        # (_, thresholded) = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)

        # Erode to get rid of unwanted noise
        erosion_size = 2;
        element = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2*erosion_size + 1, 2*erosion_size+1), (erosion_size, erosion_size))
        eroded = cv2.erode(thresholded, element)
        dilated = cv2.dilate(eroded, element)

        # Blob detection works on False elements
        keypoints = detector.detect(cv2.bitwise_not(dilated))
        with_keypoints = cv2.drawKeypoints(
            dilated,
            keypoints,
            np.array([]),
            (0,0,255),
            cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS
        )

        cv2.imshow("webcam", with_keypoints)

        # Get the positions of the detected points
        image_points = list(map(lambda p: p.pt, keypoints))
        # Sort the points in the y-direction
        image_points.sort(key = lambda p: p[1])
        image_points = np.array([image_points])

        guess = np.array([-0.08905785, -0.24945599, 0.83785317], dtype='f')
        rguess = np.array([-0.23491939, -0.05339783, 0.04889384], dtype='f')
        r = np.array([0,0,0], dtype='f')
        if len(image_points[0]) == 4:
            (ret, rvec, tvec) = cv2.solvePnP(
                tracker_points,
                image_points,
                camera_matrix,
                camera_distortion,
                # useExtrinsicGuess=True,
                # tvec=np.array([1,0,0], dtype='f'),
                # tvec=guess,
                # rvec=np.array([0,0,0], dtype='f'),
                # rvec = rguess,
                # flags = cv2.SOLVEPNP_ITERATIVE,
                flags = cv2.SOLVEPNP_P3P,
                # reprojectionError = r
            );
        else:
            if time.time() - last_print > 0.5:
                print("Unexpected amount of image points: ", len(image_points))
                last_print = time.time();


        if cv2.waitKey(1) == 27:
            break




if __name__ == "__main__":
    # capture_calibration()
    # calibrate()
    run_tracker()
