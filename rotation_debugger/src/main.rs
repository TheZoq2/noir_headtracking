use std::net::UdpSocket;

use nalgebra as na;
use na::{
    Isometry3,
    Matrix3,
    Matrix4,
    Point2,
    Point3,
    Vector3,
    Translation3,
};
use kiss3d::camera::Camera;
use kiss3d::context::Context;
use kiss3d::light::Light;
use kiss3d::resource::{Effect, Material, Mesh, ShaderAttribute, ShaderUniform};
use kiss3d::scene::ObjectData;
use kiss3d::window::Window;
use kiss3d::text::Font;
use kiss3d::event::{Action, WindowEvent, Key};

use byteorder::{ByteOrder, LittleEndian};

fn main() {
    let mut window = Window::new("Kiss3d: cube");

    // plane.set_color(1.0, 0.0, 0.0);

    window.set_light(Light::StickToCamera);

    // let rot = UnitQuaternion::from_axis_angle(&Vector3::x_axis(), 3.14/2.);


    let mut socket = UdpSocket::bind("0.0.0.0:4242").unwrap();
    socket.set_nonblocking(true).unwrap();


    let mut last_rotation = None;
    let mut last_position = None;
    'main: loop {
        // Receives a single datagram message on the socket. If `buf` is too small to hold
        // the message, it will be cut off.
        let mut buf = [0; 1024];
        while let Ok((amt, src)) = socket.recv_from(&mut buf) {
            if amt < 6 * 8 {
                println!("Too few rotation matrix values");
            }
            println!("Raw data: {:?}", &buf[0..24]);
            println!("Got data");
            let mut position: na::Vector3<f32> = na::zero();
            for index in 0..3 {
                let f = LittleEndian::read_f64(&buf[index * 8 .. index * 8 + 8]);
                position[index] = f as f32;
            }
            last_position = Some(position);
            let mut euler_angles: na::Vector3<f32> = na::zero();
            let mut offset = 0;
            for index in 0..3 {
                let f = LittleEndian::read_f64(&buf[(3+index) * 8 .. (3+index) * 8 + 8]);
                euler_angles[index] = f as f32 / 180. * std::f32::consts::PI;
            }
            let rotation = na::Rotation3::from_euler_angles(
                euler_angles[1],
                euler_angles[0],
                euler_angles[2]
            );
            last_rotation = Some(rotation);
            println!("rotation: {:?}", euler_angles);
            println!("translation: {:?}", position);
        };

        if let (Some(rot), Some(pos)) = (last_rotation, last_position) {
            let pos = pos * 0.01;
            let x_vec = pos + rot * Vector3::new(1., 0., 0.);
            let y_vec = pos + rot * Vector3::new(0., 1., 0.);
            let z_vec = pos + rot * Vector3::new(0., 0., 1.);
            // let pos = Vector3::new(0., 0., 0.);
            window.draw_line(
                &x_vec.into(),
                &pos.into(),
                &Point3::new(0., 1., 1.)
            );
            window.draw_line(
                &y_vec.into(),
                &pos.into(),
                &Point3::new(1., 0., 1.)
            );
            window.draw_line(
                &z_vec.into(),
                &pos.into(),
                &Point3::new(1., 1., 0.)
            );

            let head_offset_x = 200.;
            let head_offset_z = 0.;
            let head_offset_y = 100.;
            let constelation = vec![
        Vector3::new(head_offset_x + 0.,  head_offset_y + -52.,   head_offset_z + -50.) / 500.,
        Vector3::new(head_offset_x + 0.,  head_offset_y + 0.,     head_offset_z + 0.) / 500.,
        Vector3::new(head_offset_x + 20., head_offset_y + 45.49, head_offset_z + -8.5) / 500.,
        Vector3::new(head_offset_x + 0.,  head_offset_y + 88.,    head_offset_z + -25.) / 500.,
                // Vector3::new(0., 100., 40.) / 500.,
                // Vector3::new(-20., 59.29, 23.43) / 500.,
                // Vector3::new(0., 12., 15.) / 500.,
                // Vector3::new(0., -40., 65.) / 500.,
            ];

            for point in constelation {
                window.draw_line(
                    &(pos + rot * point).into(),
                    &pos.into(),
                    &Point3::new(1., 1., 1.)
                );
            }
        }



        draw_grid(&mut window);

        window.draw_line(
            &Point3::new(0., 0., 0.),
            &Point3::new(1., 0., 0.),
            &Point3::new(1., 0., 0.)
        );
        window.draw_line(
            &Point3::new(0., 0., 0.),
            &Point3::new(0., 1., 0.),
            &Point3::new(0., 1., 0.)
        );
        window.draw_line(
            &Point3::new(0., 0., 0.),
            &Point3::new(0., 0., 1.),
            &Point3::new(0., 0., 1.)
        );

        window.render();

        for event in window.events().iter() {
            match event.value {
                WindowEvent::Close => {
                    break 'main
                }
                _ => {}
            }
        }
    }
}



fn draw_grid(window: &mut Window)
{
    for pos in 0..=10
    {
        let pos = pos as f32;
        if pos != 0.
        {
            window.draw_line(
                &Point3::new(pos / 10., 0., -0.1),
                &Point3::new(pos / 10., 0., 1.1),
                &Point3::new(0.5, 0.5, 0.5)
            );
            window.draw_line(
                &Point3::new(-0.1, 0., pos / 10.),
                &Point3::new(1.1, 0., pos / 10.),
                &Point3::new(0.5, 0.5, 0.5)
            );
        }
    }
}
