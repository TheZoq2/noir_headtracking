use std::net::{ToSocketAddrs, UdpSocket};
use std::io;
use std::f64::consts::PI;

use nalgebra::Vector3;
use byteorder::{ByteOrder, LittleEndian};
use cv_core::WorldToCamera;

pub struct Orientation {
    rotation: Vector3<f64>,
    translation: Vector3<f64>,
}

impl From<WorldToCamera> for Orientation {
    fn from(w: WorldToCamera) -> Self {
        let (rx, ry, rz) = w.0.rotation.euler_angles();
        Orientation {
            rotation: Vector3::new(rx, ry, rz) * 180. / PI,
            // rotation: Vector3::new(0., 0., 0.),
            translation: w.0.translation.vector * 100.
        }
    }
}

pub struct Opentrack {
    sock: UdpSocket,
}


impl Opentrack {
    pub fn new<A: ToSocketAddrs>(addr: A) -> io::Result<Self> {
        let sock = UdpSocket::bind("0.0.0.0:34567")?;
        sock.connect(&addr)?;
        Ok(Self {
            sock,
        })
    }

    pub fn send_pose(
        &mut self,
        orientation: Orientation,
    ) -> io::Result<()> {
        const F64_SIZE: usize = 8;
        let mut to_send = [0u8; 3 * F64_SIZE + 3 * F64_SIZE];
        LittleEndian::write_f64(&mut to_send[0 * F64_SIZE..], orientation.translation.x);
        LittleEndian::write_f64(&mut to_send[1 * F64_SIZE..], orientation.translation.y);
        LittleEndian::write_f64(&mut to_send[2 * F64_SIZE..], orientation.translation.z);
        LittleEndian::write_f64(&mut to_send[3 * F64_SIZE..], orientation.rotation.y);
        LittleEndian::write_f64(&mut to_send[4 * F64_SIZE..], orientation.rotation.x);
        LittleEndian::write_f64(&mut to_send[5 * F64_SIZE..], orientation.rotation.z);

        let mut ptr = 0;
        while ptr < to_send.len() {
            let count = self.sock.send(&to_send)?;
            ptr += count;
        }

        Ok(())
    }
}
