use std::sync::mpsc;
use std::time::Instant;

use structopt::StructOpt;
use v4l::fraction::Fraction;
use v4l::capture;
use v4l::prelude::*;
use v4l::{Format, FourCC};
use imageproc::{
    map::map_colors,
    region_labelling::{Connectivity, connected_components},
};
use image::{ImageBuffer, Rgb, Luma};
use cv_pinhole::{NormalizedKeyPoint, CameraIntrinsics};
use cv_core::{
    KeyPoint,
    CameraModel,
    FeatureWorldMatch,
    sample_consensus::Estimator,
    WorldToCamera,
    Projective,
};
use nalgebra::{
    self as na,
    geometry::{Point3, Point2},
    Vector3,
    Vector2,
};
use lambda_twist::LambdaTwist;

mod gui;
mod opentrack;
mod debug_drawing;

#[derive(StructOpt)]
struct Opt {
    #[structopt(short, long, default_value="/dev/video0")]
    device: String,
    #[structopt(short, long, about="Show the debug gui")]
    gui: bool,
    #[structopt()]
    addr: String,
    #[structopt(short, long, default_value="4242")]
    port: u16
}

const FRAMERATE: u32 = 90;

/// Takes a grayscale image and returns the average position of each unique color.
/// This should be used with the output of connected_components to find. The returned value
/// is a tuple of the size of the blob, as well as the location. The output array is sorted
/// according to the size of the corresponding blobs
///
/// The max_amount variable specifies the maximum number of blobs we'll care about. If we find
/// more colors than it, we'll ignore them. Set this to some value larger than the amount
/// of blobs you're interested in.
fn connected_component_location(
    img: ImageBuffer<Luma<u32>, Vec<u32>>,
    max_amount: usize
) -> Vec<Point2<f64>>
{
    let mut arr = vec![(0, (0, 0)); max_amount];
    for (x, y, val) in img.enumerate_pixels() {
        // val[0] is the background. We know where that is so we'll just skip that
        if val[0] != 0 && val[0] < arr.len() as u32 {
            let (ref mut count, (ref mut val_x, ref mut val_y)) = arr[val[0] as usize];
            *count += 1;
            *val_x += x;
            *val_y += y;
        }
    }

    arr.sort_by(|(c1, _), (c2, _)| c2.cmp(c1));

    arr.into_iter()
        .filter(|(c, _)| *c > 0)
        .map(|(c, (x, y))| Point2::new(x as f64 / c as f64, y as f64 / c as f64))
        .collect()
}

/// Compute the orientation of the beacon points given the list of corresponding image
/// points. Both point lists must be sorted such that the correspondence is i -> i
fn compute_orientations(
    points: Vec<Point2<f64>>,
    beacon_points: &[Point3<f64>],
    intrinsics: CameraIntrinsics
) -> Vec<WorldToCamera> {
    if points.len() >= 4 {
        let beacon_points = beacon_points
            .iter()
            .cloned()
            .map(|p| Projective::from_point(p))
            .collect::<Vec<_>>();

        let norm_points = points.into_iter()
            .map(|p| intrinsics.calibrate(KeyPoint(p)));

        // TODO: Make sure sorting is correct
        let matches = norm_points.zip(beacon_points.into_iter())
            .map(|(cam, world)| FeatureWorldMatch(cam, world))
            .collect::<Vec<_>>();

        let lt = LambdaTwist::new();

        let result = lt.estimate(matches.into_iter());

        result.into_iter().collect()
    }
    else {
        vec![]
    }
}


pub fn reproject_points(
    points: &[Point3<f64>],
    o: &WorldToCamera,
    cam_intrinsics: &CameraIntrinsics
) -> Vec<Point2<f64>> {
    points
        .iter()
        .map(|p| o.as_ref().transform_point(p))
        .map(|p| {
            NormalizedKeyPoint(p.xy() / p.z)
        })
        .map(|p| cam_intrinsics.uncalibrate(p).0)
        .collect()
}

/// Picks the orientation that best matches the truth by reprojecting the sample points and
/// comparing the 2d distance between the result and the original image points.
///
/// Requires the points to be sorted such that image_points[i] corresponds to beacon_points[i]
pub fn select_best_orientation(
    candidates: &[WorldToCamera],
    image_points: &[Point2<f64>],
    beacon_points: &[Point3<f64>],
    cam_intrinsics: &CameraIntrinsics,
) -> Option<WorldToCamera> {
    candidates
        .iter()
        .fold(None, |best, o| {
            let reprojected = reproject_points(beacon_points, o, cam_intrinsics);

            let dist: f64 = reprojected.iter().zip(image_points.iter())
                .map(|(p1, p2)| (p1 - p2).norm())
                .sum();

            if let Some((_, prev_best)) = best {
                if dist < prev_best {
                    Some((o, dist))
                }
                else {
                    best
                }
            }
            else {
                Some((o, dist))
            }
        })
        .map(|(o, _)| o.clone())
}

fn main() {
    // Extracted from the calibration results of `tracker.py`
    let cam_intrinsics = CameraIntrinsics::identity()
        .focals(Vector2::new(640.43091593, 639.35919091))
        .principal_point(Point2::new(158.76062056, 116.79870456))
        .skew(0.);

    let head_offset = Vector3::new(125., 40., -10.);
    let beacon_rotation = na::Rotation3::from_euler_angles(0., 0., -0.3);
    // let head_offset = Vector3::new(-0., 0., 0.);
    let beacon_points = [
        Point3::new(0.,  -52.,   50.),
        Point3::new(0.,  0.,     0.),
        Point3::new(20., 45.49, 8.5),
        Point3::new(0.,  88.,    25.),
    ].iter()
        .map(|p| beacon_rotation.transform_point(p))
        .map(|p| p + head_offset)
        .map(|p| p / 1000.) // To get meters
        .collect::<Vec<_>>();

    println!("1 -> 0: {}", (beacon_points[1] - beacon_points[0]).norm());
    println!("1 -> 2: {}", (beacon_points[1] - beacon_points[2]).norm());
    println!("1 -> 3: {}", (beacon_points[1] - beacon_points[3]).norm());
    println!("0 -> 3: {}", (beacon_points[0] - beacon_points[3]).norm());

    let opt = Opt::from_args();

    let mut remote = opentrack::Opentrack::new((opt.addr.as_str(), opt.port)).unwrap();

    // Allocate 4 buffers by default
    let buffers = 4;

    let path = opt.device;

    // Set up v4l
    let mut format: Format;
    let mut params: capture::Parameters;

    let mut dev = CaptureDevice::with_path(path.clone()).expect("Failed to open device");
    format = dev.format().expect("Failed to get format");
    params = dev.params().expect("Failed to get parameters");

    // enforce RGB3
    format.fourcc = FourCC::new(b"RGB3");
    format.width = 320;
    format.height = 240;
    params.interval = Fraction::new(1, FRAMERATE);
    format = dev.set_format(&format).expect("Failed to set format");
    params = dev.set_params(&params).expect("Failed to set params");

    println!("Active format:\n{}", format);
    println!("Active parameters:\n{}", params);

    // Channel for sending pictures to the GUI
    let (tx, rx) = mpsc::channel();

    // The GUI needs to be run on the main thread for winit reasons.
    if opt.gui {
        gui::run_gui((format.width, format.height), rx);
    }

    // Setup a buffer stream
    let mut stream =
        MmapStream::with_buffers(&mut dev, buffers).expect("Failed to create buffer stream");

    let mut timings = vec![];

    loop {
        let buf = stream.next().expect("Failed to capture buffer");
        let start = Instant::now();

        let img = ImageBuffer::<Rgb<u8>, _>::from_raw(format.width, format.height, buf.to_vec())
            .expect("Failed to create image buffer");

        // We could use `threshold(|p| p.as_luma)` here, but that is much much slower than this
        let thresholded = map_colors(&img, |p| {
            let threshold = 165;
            if p[0] > threshold && p[1] > threshold && p[2] > threshold {
                Luma([255 as u8])
            } else {
                Luma([0 as u8])
            }
        });

        let conn = connected_components(&thresholded, Connectivity::Eight, Luma([0]));
        let mut locations = connected_component_location(conn, 100);

        // Sort the points with the top point first
        locations.sort_by(|p1, p2| {
            p1.y.partial_cmp(&p2.y).unwrap_or(std::cmp::Ordering::Equal)
        });
        let orientations = compute_orientations(locations.clone(), &beacon_points, cam_intrinsics);

        let orientation = select_best_orientation(
            &orientations,
            &locations,
            &beacon_points,
            &cam_intrinsics
        );
        orientation
            .map(From::from)
            .map(|x| {
                remote.send_pose(x).unwrap();
            });

        timings.push(start.elapsed().as_secs_f64());

        if timings.len() > FRAMERATE as usize {
            let total: f64 = timings.iter().sum();

            let per_frame = total / timings.len() as f64;

            println!("Average frame time: {}. {} FPS", per_frame, 1. / per_frame);
            timings.clear();
        }

        if opt.gui {
            // let gray_rgb = map_colors(&thresholded, |p| p.to_rgb());
            let mut to_draw = img;

            debug_drawing::debug_drawing(
                &mut to_draw,
                &locations,
                &beacon_points,
                &cam_intrinsics,
                &orientation.map(|o| vec![o]).unwrap_or_else(|| vec![])
            );

            let data = to_draw.as_raw().to_vec();
            tx.send(data).unwrap();
        }
    }
}

