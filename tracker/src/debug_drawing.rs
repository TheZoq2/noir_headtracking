use nalgebra::{Point3, Point2};

use image::{Pixel, Rgb};
use imageproc::drawing::{self, Canvas};
use cv_core::WorldToCamera;
use cv_pinhole::CameraIntrinsics;


pub fn debug_drawing<C>(
    c: &mut C,
    image_points: &Vec<Point2<f64>>,
    beacon_points: &[Point3<f64>],
    cam_intrinsics: &CameraIntrinsics,
    orientations: &[WorldToCamera],
)
    where C: Canvas<Pixel = Rgb<u8>>,
{

    let color_seq = [
        Rgb([0, 0, 255]),
        Rgb([0, 255, 0]),
        Rgb([0, 255, 255]),
        Rgb([255, 0, 0]),
        Rgb([255, 0, 255]),
        Rgb([255, 255, 0]),
        Rgb([255, 255, 255]),
    ];

    for (i, p) in image_points.into_iter().enumerate() {
        let (x, y) = (p.x, p.y);
        if x != 0. && y != 0. {
            let color = color_seq[i % color_seq.len()];
            drawing::draw_cross_mut(c, color, x as i32, y as i32);
        }
    }

    // Reproject things for debugging
    for (i, o) in orientations.iter().enumerate() {
        let transformed = crate::reproject_points(beacon_points, o, cam_intrinsics);
        for p in transformed {
            let color = color_seq[i % color_seq.len()];
            let (x, y) = (p.x, p.y);
            draw_cross(c, color, x as i32, y as i32);
        }

        let origin = crate::reproject_points(&[Point3::new(0., 0., 0.)], o, cam_intrinsics);
        let axes = &[Point3::new(0.2, 0., 0.), Point3::new(0., 0.2, 0.), Point3::new(0., 0., 0.2)];
        let axes_proj = crate::reproject_points(axes, o, cam_intrinsics);
        let colors = &[Rgb([255, 0, 0]), Rgb([0, 255, 0]), Rgb([0, 0, 255])];
        for (p, color) in axes_proj.into_iter().zip(colors.iter()) {
            let start = (origin[0].x as f32, origin[0].y as f32);
            let end = (p.x as f32, p.y as f32);
            drawing::draw_line_segment_mut(c, start, end, *color);
        }
    }
}

pub fn draw_cross<C, P>(c: &mut C, color: P, x: i32, y: i32) 
    where C: Canvas<Pixel = P>,
          P: Pixel
{
    for x_off in -3..3 {
        let pos = x_off + x;
        draw_pixel_in_bounds(c, pos, y, color);
    }
    for y_off in -3..3 {
        let pos = y_off + y;
        draw_pixel_in_bounds(c, x, pos, color)
    }
}

fn draw_pixel_in_bounds<C, P>(c: &mut C, x: i32, y: i32, color: P)
    where C: Canvas<Pixel = P>,
          P: Pixel
{
    if x > 0 && y > 0 && x < c.width() as i32 && y < c.height() as i32 {
        c.draw_pixel(x as u32, y as u32, color);
    }
}
